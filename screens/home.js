import React from 'react';
import {View,Text,TextInput,Button} from 'react-native';

export default class Home extends React.Component{
    constructor(props){
        super(props);
        this.state={
            nom:''
        }
    }

    
    render(){
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                   <TextInput placeholder='Enter username' onChangeText={(change)=>this.setState({nom:change})}/>
                   <Button title='Save' onPress={()=>this.props.navigation.navigate('Details',{'nom':this.state.nom})}/>
                    
            </View>
        )
    }
}