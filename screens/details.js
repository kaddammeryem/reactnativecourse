import React from 'react';
import {View,Button,Text} from 'react-native';


export default class Details extends React.Component{
    render(){
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Text>{this.props.route.params.nom}</Text>
                    <Button title="goToSettings"onPress={()=>this.props.navigation.navigate('Settings')}/> 
                    <Button title="goBack" onPress={()=>this.props.navigation.goBack()}/> 
            </View>
        )
    }
}