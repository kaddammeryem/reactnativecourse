import React from 'react';
import {View,Button,Text} from 'react-native';


export default class Settings extends React.Component{
    
    render(){
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Text>Hello SETTINGS</Text>
                    <Button title="goToDetails" onPress={()=>this.props.navigation.navigate('Details',{nom:this.props.route.params.nom})}/> 
                    <Button title="goBack" onPress={()=>this.props.navigation.navigate('Home')}/> 
            </View>
        )
    }
}